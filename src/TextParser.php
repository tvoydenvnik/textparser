<?php

namespace Tvoydenvnik\TextParser;

use Tvoydenvnik\Common\Lib\JSON;

class TextParser {

    function wrapTag($sText, $sTag, $attrs = ""){
        return "<$sTag$attrs>$sText</$sTag>";
    }

    function getRegex($sTag)
    {
        return 
            '#\['.$sTag.'(?:=(?P<val>[^\s\]]*))?(?P<params>[^\]]*)\]'.
            '(?P<content>(?:(?!\[/?'.$sTag.'\W).)*)'.
            '\[/'.$sTag.'\]#isu';
    }

    function replaceBBcode($sText, $sTag, $fCallback, $trim=true){  
        $regex = $this->getRegex($sTag);
        
        $count = 0;
        do{
            $sText = preg_replace_callback($regex, function($m) use(&$fCallback, $sTag, $trim){
                $paramsLst = preg_grep("/\w+=\w+/", preg_split('/\s+/', trim($m['params'])));
                $params = array();
                foreach ($paramsLst as $param) {
                    $param = explode('=', $param);
                    $params[strtolower($param[0])] = $param[1];
                }

                $content = $m['content']; 
                
                if($trim){
                    $content = trim($content);
                }

                if($fCallback){
                    $content = $fCallback($m['val'], $params, $content);
                }
                else{
                    $content = $this->wrapTag($content, $sTag);
                }

                return $content;
            }, $sText, -1, $count);
        } while($count > 0);

        return $sText;
    }


    /*
     * Удалить все bbcode теги из текста. Как начальные, конечные, самозакрывающиеся.
     */
    public function clearBBCode($sText){
        return preg_replace("/\[[^\]]+\]/", "", $sText);
    }


    /*
    * Lists
    */
    public function BBCodeListToHtml($sText){
        return $this->replaceBBcode($sText, 'list', function($value, $params, $c){
            $content = "";
            foreach (array_slice(explode("[*]", $c), 1) as $item) {
                $content .= $this->wrapTag(trim($item), 'li');
            }
            $l = $value == "1" ? "ol" : "ul";
            return $this->wrapTag($content, $l, " class=\"bbcode-$l\"");
        });
    }

    /*
     * Таблицы
     * [TABLE][TR][TD]Строка 1.1[/TD][TD]Строка 1.2[/TD][TD]Строка 1.3[/TD][/TR][TR][TD]Строка 2.1[/TD][TD]Строка 2.2[/TD][TD]Строка 2.3[/TD][/TR][/TABLE]
     * <table class="bbcode-table">...
     */
    public function BBCodeTableToHtml($sText){
        return $this->replaceBBcode($sText, 'table', function($value, $params, $content){
            $content = preg_replace('/(?:\[\/tr\])\s+(?:\[tr\])/is', '[/tr][tr]', $content);
            $content = preg_replace('/(?:\[\/td\])\s+(?:\[td\])/is', '[/td][td]', $content);
            $content = $this->replaceBBcode($content, 'tr', null);
            $content = $this->replaceBBcode($content, 'td', null);
            return $this->wrapTag($content, 'table', ' class="bbcode-table"');
        });
    }


    /*
    *
    * [QUOTE]Цитата[/QUOTE]
    *
    * <div class="bbcode-quote">Цитата</div>
    */
    public function BBCodeQuoteToHtml($sText){
        return $this->replaceBBcode($sText, 'quote', function($value, $params, $content){
            return $this->wrapTag($content, 'div', ' class="bbcode-quote"');
        });
    }


    /*
    * Упоминание
    * [USER=101854]Светлана[/USER]
    * <a class="bbcode-mention" href="/people/user/101854/">Светлана</a>
    */
    public function BBCodeMentionToHtml($sText){
        return $this->replaceBBcode($sText, 'user', function($value, $params, $content){
            return $this->wrapTag($content, 'a', ' class="bbcode-mention" href="/people/user/'.$value.'/"');
        });
    }

    /*
    * #ХэшТег
    * >
    * <a class="bbcode-hashtag" href="/hashtag/ХэшТег/">#ХэшТег</a>
    */

    public function BBCodeHashTagToHtml($sText){
        return preg_replace('/#([\p{L}_]*)/u', '<a class="bbcode-hashtag" href="/hashtag/\1/">#\1</a>', $sText);
    }

    /*
    * Ссылки:
    *
    *
    [URL=http://health-diet.ru/]Ссылка[/URL]
    >
    <a class="bbcode-a" href="/away.php?to=url">Ссылка</a>
    Если текст ссылки больше 30 знаков, то он обрезается: Длинный текст...
    */

    public function BBCodeHrefToHtml($sText){
       return $this->replaceBBcode($sText, 'url', function($value, $params, $content){
            $content = preg_replace('/\n/s', '', $content);
            return $this->wrapTag($content, 'a', ' class="bbcode-a" href="/away.php?to='.urlencode($value).'"');
        });
    }

    /*
     *  Ссылки
    http://health-diet.ru/
    >
    <a class="bbcode-a" href="/away.php?to=url">http://health-diet.ru/</a>
    Если длина ссылки больше 30 знаков, то он обрезается: http://health-diet.ru/...
     */

    public function textHrefToHtml($sText){
        return preg_replace_callback('#(((https?|ftp)://[\p{L}-_\.%]+)(?P<res>/[\p{L}-_/\.&;=+%]*)?)#u', function($m){
            if (isset($m['res']) && strlen($m[1]) > 30){
                $m[0] = $m[2] . '/...';
            }
            return $this->wrapTag($m[0], 'a', ' class="bbcode-a" href="/away.php?to='.urlencode($m[1]).'"');
        }, $sText);
    }


    /**
     * Удалить из текста переводы строк, которых подряд более 2.
     * @param $sText
     */
    public function textRemoveBr($sText){
        return preg_replace('/\n\n+/s', '\n', $sText);
    }


    /*
    * Изображения
    * Вариант 1.
    * [IMG WIDTH=120 HEIGHT=120]http://159523.selcdn.ru/upload/resize_cache/237400/41362a91468df82910692010106f4f66/main/53c/53c4a9a1d9b6dcecff2c62f70d8dc9be/3d9791bca62ad22446881a0413268ca8.jpg[/IMG]
    * >
    * <img class="bbcode-photo" style="width, height"  src="Путь к изображению"/>
    *
    * Вариант 2.
    *
    * [IMG ID=74]
    * >
    * <img class="bbcode-photo" style="width, height" data-src="74" src="Путь из массива"/>
    *
    */
    public function BBCodeImageToHtml($sText, array $arImageSrc = array()){
        $sText = preg_replace_callback('/\[IMG\s+ID=(\d+)\]/i', function($m) use(&$arImageSrc) {
            if (!isset($arImageSrc[$m[1]])) return "";

            return '<img class="bbcode-photo" data-src="'
                .$m[1].'" src="'.$arImageSrc[$m[1]].'"/>';
        }, $sText);

        return $this->replaceBBcode($sText, 'img', function($val, $params, $content){
            return '<img class="bbcode-photo" '
                .'style="width:'.$params['width'].'px; height:'.$params['height'].'px" '
                .'src="'.$content.'"/>';
        });
    }


    /**
     *
     * Находит в тексте коды всех картинок.
     * Например, если в тексте есть [IMG ID=74] [IMG ID=123], то вернется [74, 123]
     * @param $sText
     */
    public function getImages($sText){
        $matches = array();
        preg_match_all('/\[IMG\s+ID=(\d+)\]/', $sText, $matches);
        return $matches[1];
    }

    /*
     * Видео
     *
     * [VIDEO  WIDTH=459 HEIGHT=344]https://www.youtube.com/embed/ZVRTUSYErq4?feature=oembed[/VIDEO]
     * >
     *
     * Если это youtube, vimeo, rutube то формируется код для вставки (iframe)
     * Иначе ссылка
     * <a href="/away.php?to=url" class="bbcode-video" ></a>
     */
    public function BBCodeVideoToHtml($sText){
        return $this->replaceBBcode($sText, 'video', function($val, $params, $content){
            if (preg_match('#^https?://www\.(youtube\.com|vimeo\.com|rutube\.ru)#', $content)){
                return '<iframe width="'.$params['width'].'" height="'.$params['height'].'" '
                    .'src="'.urlencode($content).'" allowfullscreen></iframe>';
            }
            else {
                return '<a href="/away.php?to='.urlencode($content).'" class="bbcode-video" ></a>';
            }
        });
    }


    /*
    * Смайлы
    *
    * Заменить все смайлы. Например, :) ;) :D )
    * >
    * <img class="bbcode-smile" src="Путь к смайлу"/>
    */
    public function textSmilesToHtml($sText, array $arSmileConfig){
        foreach ($arSmileConfig as $smile => $url) {
            $sText = str_replace($smile, '<img class="bbcode-smile" src="'.$url.'"/>', $sText);
        }
        return $sText;
    }

    /*
     * Удалить все html теги
     */
    public function clearHTMLTags($sText){
        return preg_replace('/<[^>]*>/s', '', $sText);
    }


    /*
     * Добавить тег br вместо \n
     * Если \n больше 3, то они удаляются
     */
    public function addBr($sText){
        return preg_replace('/\n/s', '<br />', preg_replace('/(\n\s*){2,}\n/s', "\n\n", $sText));
    }


    /**
     * Обработка сообщения, перед сохранение в БД: удаляем все html теги.
     * @param $pMessage - строка и array(json)
     * @param int $nLength
     * @return string
     */
    public function prepareForSaveInDB($pMessage, $nLength=0){

        $isArray = false;
        if(is_array($pMessage)){
            $pMessage = JSON::json_encode($pMessage);
            $isArray = true;
        }

        $pMessage = trim($pMessage);


        if ($nLength) $pMessage = substr($pMessage, 0, $nLength);

        $pMessage = $this->clearHTMLTags($pMessage);


        if($isArray === true){
            return JSON::json_decode($pMessage);
        }else{
            return $pMessage;
        }

    }


    /**
     * Проверить текст на наличие вхождений слов из $arForbidden
     * Достаточно одного вхождения
     * @param $sText
     * @param array $arForbidden
     * @return bool
     */

    public function hasForbiddenText($sText, array $arForbidden){
        foreach ($arForbidden as $word) {
            if (!ctype_alnum($word[0])){
                if (preg_match($word, $sText))
                    return true;
            } else{
                if(strpos($sText, $word) !== false)
                    return true;
            }
        }
        return false;
    }

    /**
     * Конвертировать текст в html.
     * Исходный текст может содержать bbcode
     * @param $sText
     * @return mixed
     */
    public function toHtml($sText, $arImageSrc = array(), $arSmileConfig = array()){

        $result = trim($sText);


        $result = $this->clearHTMLTags($result);


        /*
         * Если текст содержит bbcode (проверить на наличие bbcode тегов ), то конвертируем их.
         * Если bbcode тегов, нет в тексте, то не запускать алгоритм
         */
        if($this->hasBBCode($sText)){

            $result = $this->BBCodeBUIToHtml($sText);
            $result = $this->BBCodeListToHtml($result);
            $result = $this->BBCodeTableToHtml($result);
            $result = $this->BBCodeMentionToHtml($result);
            $result = $this->BBCodeHashTagToHtml($result);
            $result = $this->BBCodeHrefToHtml($result);
            $result = $this->BBCodeVideoToHtml($result);
            $result = $this->BBCodeImageToHtml($result, $arImageSrc);
            $result = $this->addBr($result);
            $result = $this->textSmilesToHtml($result, $arSmileConfig);

            /*
             * Удалим прочие bbcode теги
             */
            $result = $this->clearBBCode($result);

        }


        $result = $this->textHrefToHtml($result);

        // $result = $this->tidyCheck($result);

        return $result;
    }


    public function messageParse($sText){

        //

    }

    /**
     *
     * Преобразуем текст в обычный текст.
     * @param $sText
     * @param int $nLength
     * @return String
     */
    public function toSimpleText($sText, $nLength=0){

        $result = $this->clearBBCode($sText);

        $result = $this->clearHTMLTags($result);

        $result = preg_replace_callback('#(((https?|ftp)://[\p{L}-_\.%]+)(?P<res>/[\p{L}-_\.&;=+%]*)?)#u', function($m){
            if (isset($m['res']) && strlen($m[1]) > 30){
                $m[0] = $m[2] . '/...';
            }
            return $m[0];
        }, $result);

        if ($nLength) $result = substr($result, 0, $nLength);

        return $result;
    }


    /**
     * С помощью расширения Tidy проверить текст на наличие всех тегов
     * @param $sText
     * @return bool
     */
    public function tidyCheck($sText){
        $config = array(
            //'indent'         => true,
            'clean' => true,
            'output-xhtml'   => true,
            'show-body-only' => true,
            //'wrap'           => 0
        );

        $Tidy = new \tidy();
        $Tidy->parseString($sText, $config, 'utf8');
        $Tidy->cleanRepair();

        return rtrim(tidy_get_output($Tidy));

    }


    /**
     * Текст сожержит BBCode
     * @param $sText
     * @return bool
     */
    public function hasBBCode($sText){

        return preg_match('/\[.*\]/', $sText) != 0;
    }

    /*
       *
       * [B]Жирный[/B]
       * >
       * <span class="bbcode-b">Жирный</span>
       *
       * [I]Курсив[/I]
       * >
       * <span class="bbcode-i">Курсив</span>
       *
       * [U]Подчеркнутый[/u]
       * <span class="bbcode-u">Подчеркнутый</span>
       *
      */
    public function BBCodeBUIToHtml($sText){
        $sText = preg_replace( "#\[b\](.+?)\[/b\]#is", "<span class=\"bbcode-b\">\\1</span>", $sText );
        $sText = preg_replace( "#\[i\](.+?)\[/i\]#is", "<span class=\"bbcode-i\">\\1</span>", $sText );
        $sText = preg_replace( "#\[u\](.+?)\[/u\]#is", "<span class=\"bbcode-u\">\\1</span>", $sText );

        return $sText;
    }


    /*
     * Возвращает массив упомянутых пользователей в тексте
     * Формат упоминания: [USER=101854]Светлана[/USER]
     * @param $sText
     */
    public function getMentions($sText){
        $data = array();
        preg_replace_callback("/\[user=(.+?)\](.+?)\[\/user\]/i", function($m) use (&$data) {
            $data[] = [
                'id' => (int) $m[1],
                'name' => $m[2],
            ];
        }, $sText);

        return $data;
    }

}