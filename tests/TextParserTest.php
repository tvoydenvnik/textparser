<?php

namespace Tvoydenvnik\TextParser\Tests;

use Tvoydenvnik\TextParser\TextParser;


class TextParserTest  extends \PHPUnit_Framework_TestCase {

    private $parser;

    public function __construct() {
        $this->parser = new TextParser();
    }

    /**
     * см. TextParser->getMentions
     */
    public function testGetMentions(){

        $bbcode = <<<EOD
Lorem ipsum [user=101854]Светлана[/user]
and some [user=51442]Vitaliy[/UseR] checked this test!
EOD;

        $result = array(
            array(
                'id' => 101854,
                'name' => 'Светлана',
            ),
            array(
                'id' => 51442,
                'name' => 'Vitaliy',
            )
        );

        $this->assertEquals($result, $this->parser->getMentions($bbcode));
    }

    public function testClearBBCode(){
        //см. TextParser->clearBBCode
    }

    public function testBBCodeBUIToHtml(){


        $bbcode = <<<EOD
Lorem [B]ipsum[/b]
[i]Светлана[/I] and some [u]Vitaliy[/u] checked this test!
EOD;

        $html = <<<EOD
Lorem <span class="bbcode-b">ipsum</span>
<span class="bbcode-i">Светлана</span> and some <span class="bbcode-u">Vitaliy</span> checked this test!
EOD;


        $this->assertEquals(
            $html,
            $this->parser->BBCodeBUIToHtml($bbcode)
        );

    }


    public function testBBCodeListToHtml(){
        $bbcode = <<<EOD
[LIST]
    [*]l1
    [*][LIST=1]
        [*]ol 1
        [*]ol 2
        [*]ol 3
    [/LIST]
[/list]
EOD;
        $html = '<ul class="bbcode-ul">'.
                    '<li>l1</li>'.
                    '<li>'.
                        '<ol class="bbcode-ol"><li>ol 1</li><li>ol 2</li><li>ol 3</li></ol>'.
                    '</li>'.
                '</ul>';
        $this->assertEquals(
            $html,
            $this->parser->BBCodeListToHtml($bbcode)
        );
    }


    public function testBBCodeTableToHtml(){
        $bbcode = <<<EOD
[TABLE]
[TR][TD]Строка 1.1[/TD]
    [TD]Строка 1.2[/TD]
    [TD]Строка 1.3[/TD][/TR]
[TR][TD]Строка 2.1[/TD]
    [TD]Строка 2.2[/TD]
    [TD]Строка 2.3[/TD]
[/TR]
[/TABLE]
EOD;
        $html =
            '<table class="bbcode-table">'.
            '<tr><td>Строка 1.1</td>'.
                '<td>Строка 1.2</td>'.
                '<td>Строка 1.3</td></tr>'.
            '<tr><td>Строка 2.1</td>'.
                '<td>Строка 2.2</td>'.
                '<td>Строка 2.3</td>'.
            '</tr>'.
            '</table>';
        $this->assertEquals(
            $html,
            $this->parser->BBCodeTableToHtml($bbcode)
        );
    }

    public function testBBCodeQuoteToHtml(){
        $this->assertEquals(
            '<div class="bbcode-quote">Цитата</div>',
            $this->parser->BBCodeQuoteToHtml('[QUOTE]Цитата[/QUOTE]')
        );
    }

    public function testBBCodeMentionToHtml(){
        $this->assertEquals(
            '<a class="bbcode-mention" href="/people/user/101854/">Светлана</a>',
            $this->parser->BBCodeMentionToHtml('[USER=101854]Светлана[/USER]')
        );
    }
    public function testBBCodeHashTagToHtml(){
        $this->assertEquals(
            'Here is <a class="bbcode-hashtag" href="/hashtag/Хэш_Тег/">#Хэш_Тег</a>!',
            $this->parser->BBCodeHashTagToHtml('Here is #Хэш_Тег!')
        );
    }

    public function testBBCodeHrefToHtml(){
        $this->assertEquals(
            '<a class="bbcode-a" href="/away.php?to=http%3A%2F%2Fhealth-diet.ru%2F">Ссылка</a>',
            $this->parser->BBCodeHrefToHtml('[URL=http://health-diet.ru/]Ссылка[/URL]')
        );
    }

    public function testTextHrefToHtml(){
        $this->assertEquals(
            '<a class="bbcode-a" href="/away.php?to=http%3A%2F%2Fhealth-diet.ru%2F">http://health-diet.ru/</a>',
            $this->parser->textHrefToHtml('http://health-diet.ru/')
        );

        $html = <<<EOD
Вот ссылка диет: http://health-diet.ru/
Вот ещё одна:// http://здороваядиета.рф
Важно: http://здороваядиета.рф/здоровая_диетаи_ногда_бывает_полезной
EOD;
        $href = <<<EOD
Вот ссылка диет: <a class="bbcode-a" href="/away.php?to=http%3A%2F%2Fhealth-diet.ru%2F">http://health-diet.ru/</a>
Вот ещё одна:// <a class="bbcode-a" href="/away.php?to=http%3A%2F%2F%D0%B7%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D0%B0%D1%8F%D0%B4%D0%B8%D0%B5%D1%82%D0%B0.%D1%80%D1%84">http://здороваядиета.рф</a>
Важно: <a class="bbcode-a" href="/away.php?to=http%3A%2F%2F%D0%B7%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D0%B0%D1%8F%D0%B4%D0%B8%D0%B5%D1%82%D0%B0.%D1%80%D1%84%2F%D0%B7%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D0%B4%D0%B8%D0%B5%D1%82%D0%B0%D0%B8_%D0%BD%D0%BE%D0%B3%D0%B4%D0%B0_%D0%B1%D1%8B%D0%B2%D0%B0%D0%B5%D1%82_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B7%D0%BD%D0%BE%D0%B9">http://здороваядиета.рф/...</a>
EOD;
        $this->assertEquals(
            $href,
            $this->parser->textHrefToHtml($html)
        );
    }

    public function testBBCodeImageToHtml(){
        $img = '[IMG WIDTH=120 HEIGHT=240]http://159523.selcdn.ru/upload'
            .'/resize_cache/237400/41362a91468df82910692010106f4f66/'
            .'main/53c/53c4a9a1d9b6dcecff2c62f70d8dc9be/3d9791bca62ad22446881a0413268ca8.jpg[/IMG]';
        $this->assertEquals(
            '<img class="bbcode-photo" style="width:120px; height:240px" '
                .'src="http://159523.selcdn.ru/upload/resize_cache/237400/'
                .'41362a91468df82910692010106f4f66/main/53c/53c4a9a1d9b6dc'
                .'ecff2c62f70d8dc9be/3d9791bca62ad22446881a0413268ca8.jpg"/>',
            $this->parser->BBCodeImageToHtml($img)
        );

        $bb = '[img id=555][img id=666]';
        $arr = [
            '555' => '/123/345'
        ];
        $this->assertEquals(
            '<img class="bbcode-photo" data-src="555" src="/123/345"/>',
            $this->parser->BBCodeImageToHtml($bb, $arr)
        );
    }

    public function testBBCodeVideoToHtml(){
        $iframe = '<iframe width="640" height="360" '.
            'src="https%3A%2F%2Fwww.youtube.com%2Fembed%2Fqwertyuiop%3Ffeature%3Dembed" allowfullscreen></iframe>';

        $this->assertEquals(
            $iframe,
            $this->parser->BBCodeVideoToHtml('[VIDEO  WIDTH=640 HEIGHT=360]'
                .'https://www.youtube.com/embed/qwertyuiop?feature=embed[/VIDEO]')
        );
        $this->assertEquals(
            '<a href="/away.php?to=https%3A%2F%2Fvideo.com%2F12334567" class="bbcode-video" ></a>',
            $this->parser->BBCodeVideoToHtml('[VIDEO  WIDTH=640 HEIGHT=360]'
                .'https://video.com/12334567[/VIDEO]')  
        );
    }

    public function testClearHTMLTags(){
        $this->assertEquals($this->parser->ClearHTMLTags('<div class="gb_1a"><div class="gb_ha gb_ga gb_ua" aria-label="Приложения Google" aria-hidden="true" role="region"><ul class="gb_ja gb_ca" aria-dropeffect="move"><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="192" href="https://myaccount.google.com/?utm_source=OGB" id="gb192" data-ved="0EMEuCAAoAA"><div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -1449px"></span><span class="gb_4">Мой аккаунт</span></a></li><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="1" href="https://www.google.ru/webhp?tab=ww&amp;ei=3uYXV4SJE8G2sQHDl5TgDw&amp;ved=0EKkuCAEoAQ" id="gb1"><div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -276px"></span><span class="gb_4">Поиск</span></a></li><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="8" href="https://maps.google.ru/maps?hl=ru&amp;tab=wl" id="gb8" data-ved="0EMEuCAIoAg"><div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -69px"></span><span class="gb_4">Карты</span></a></li></ul>'), 'Мой аккаунтПоискКарты');
    }


    public function testToSimpleText(){
        $this->assertEquals(
            'Мой аккаунт https://video.com/12334567 ПоискКарты http://здороваядиета.рф/...',
            $this->parser->toSimpleText('<ul class="gb_ja gb_ca" aria-dropeffect="move"><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="192" href="https://myaccount.google.com/?utm_source=OGB" id="gb192" data-ved="0EMEuCAAoAA"><div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -1449px"></span><span class="gb_4">Мой аккаунт </span></a></li><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="1" href="https://www.google.ru/webhp?tab=ww&amp;ei=3uYXV4SJE8G2sQHDl5TgDw&amp;ved=0EKkuCAEoAQ" id="gb1">[VIDEO  WIDTH=640 HEIGHT=360]https://video.com/12334567[/VIDEO] <div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -276px"></span><span class="gb_4">Поиск</span></a></li><li class="gb_Z" aria-grabbed="false"><a class="gb_O" data-pid="8" href="https://maps.google.ru/maps?hl=ru&amp;tab=wl" id="gb8" data-ved="0EMEuCAIoAg"><div class="gb_8"></div><div class="gb_9"></div><div class="gb_aa"></div><div class="gb_ba"></div><span class="gb_3" style="background-position:0 -69px"></span><span class="gb_4">Карты</span></a> http://здороваядиета.рф/здоровая_диетаи_ногда_бывает_полезной</li></ul>')
        );
    }


//    public function testTidyCheck(){
//
//        $this->assertEquals("<p>text</p>",$this->parser->tidyCheck("<p>text</i>"));
//
//        $tableWrong = <<<EOD
//<table>
//<tr>
//<td>table</td>
//</tf>
//</table>
//EOD;
//
//        $tableFix = <<<EOD
//<table>
//<tr>
//<td>table</td>
//</tr>
//</table>
//EOD;
//
//        $this->assertEquals($tableFix,$this->parser->tidyCheck($tableWrong));
//
//    }

    public function testGetImages(){
        $this->assertEquals(
            array_fill_keys([22, 43, 55], 1), 
            array_fill_keys($this->parser->getImages("Lorem ipsum dolor sit amet, [IMG ID=43][IMG ID=55]consectetur adipiscing elit. [IMG ID=22]Phasellus tincidunt suscipit nibh eu hendrerit. Pellentesque suscipit lorem mauris, vel tincidunt lectus hendrerit at."), 1));
    }

    public function testHasForbiddenText(){
        //см. TextParser->hasForbiddenText
        $arForbidden = array(
            "ololo",
            "www.xxx.ru",
            "/[хХ][уУ][йЙ](ня)?/u"
        );
        $texts = array(
            'вот и умер дед максим' => 0,
            'да и хер остался с ним' => 0,
            'с ololo ты не балуй' => 1,
            '********хуй*********' => 1,
            '(ня)' => 0,
            'хуЙня' => 1
        );
        foreach ($texts as $text => $forbidden) {
            $this->assertEquals(
                !!$forbidden,
                $this->parser->hasForbiddenText($text, $arForbidden)
            );
        }
    }



    public function testHasBBCode(){

        $this->assertEquals(true, $this->parser->hasBBCode("Lorem ipsum dolor sit amet, consectetur adipiscing elit. [IMG ID=22]Phasellus tincidunt suscipit nibh eu hendrerit. Pellentesque suscipit lorem mauris, vel tincidunt lectus hendrerit at."));
        $this->assertEquals(false, $this->parser->hasBBCode("Lorem ipsum dolor sit amet, consectetur adipiscing elit. [IMG Phasellus tincidunt suscipit nibh eu hendrerit. Pellentesque suscipit lorem mauris, vel tincidunt lectus hendrerit at."));

    }


    public function testTextSmilesToHtml(){
        //см. TextParser->textSmilesToHtml
        $smiles = [
            ':)' => 'smiley.jpg',
            ':>' => 'evil.jpg',
            ';-)' => 'wink.jpg',
            ':-)' => 'smiley.jpg'
        ];
        $this->assertEquals('WOW SUCH A 7 NICE SMILES <img class="bbcode-smile" src="smiley.jpg"/>)))) :<img class="bbcode-smile" src="smiley.jpg"/>(<img class="bbcode-smile" src="smiley.jpg"/>)))<img class="bbcode-smile" src="smiley.jpg"/>:----)<img class="bbcode-smile" src="smiley.jpg"/><img class="bbcode-smile" src="evil.jpg"/>:->:<<img class="bbcode-smile" src="wink.jpg"/>',
            $this->parser->textSmilesToHtml('WOW SUCH A 7 NICE SMILES :))))) ::)(:)))):):----):-):>:->:<;-)', $smiles));
    }

    public function testAddBr(){
        $this->assertEquals("text<br /><br /> test<br /><br />text<br />",
            $this->parser->AddBr("text\n \n\t \n\n test\n\ntext\n"));
    }



    public function testToHtml(){
        $text = <<<EOD
[list]
    [*][table]
        [tr]    
            [td]John[/td]
            [td]65[/td]
        [/tr]
        [tr]
            [td]Gitte[/td]
            [td]40[/td]
        [/tr]
        [tr]
            [td]Sussie[/td]
            [td]19[/td]
        [/tr]
    [/table]
    [*]http://cornhub.com
    [*]seen this one? [VIDEO  WIDTH=640 HEIGHT=360]https://www.youtube.com/embed/qwertyuiop?feature=embed[/VIDEO]
[/list]
EOD;
        $result =
            '<ul class="bbcode-ul">'.
                '<li>'.
                    '<table class="bbcode-table">'.
                        '<tr>'.
                            '<td>John</td>'.
                            '<td>65</td>'.
                        '</tr>'.
                            '<tr>'.
                            '<td>Gitte</td>'.
                            '<td>40</td>'.
                        '</tr>'.
                        '<tr>'.
                            '<td>Sussie</td>'.
                            '<td>19</td>'.
                    '</tr>'.
                '</table>'.
                '</li>'.
                '<li><a class="bbcode-a" href='.
                '"/away.php?to=http%3A%2F%2Fcornhub.com">http://cornhub.com</a></li>'.
                '<li>seen this one? <iframe width="640" height="360" '.
                    'src="https%3A%2F%2Fwww.youtube.com%2Fembed%2Fqwertyuiop%3Ffeature%3Dembed" allowfullscreen>'.
                '</iframe></li>'.
            '</ul>';

        $this->assertEquals($result,
            $this->parser->toHtml($text));
    }

}

